﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Thief : Hero
    {
        public bool canBackStab = true;

        public Thief()
        {
            this.attack = 12;
            this.defense = 6;
        }
        public Thief (float atk, float def, float spd) : base(atk, def, spd) { }

        public void Stealth()
        {
            if (canBackStab)
            {
                this.attack *= 1.5f;
                this.speed *= 1.5f;
            }
        }

        public float BackStab()
        {
            canBackStab = false;
            return attack * 1.25f;
        }

    }

}
