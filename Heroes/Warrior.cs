﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    public class Warrior : Hero
    {
        public Warrior()
        {
            this.attack = 13;
            this.defense = 10;
        }

        public Warrior(float atk, float def, float spd) : base(atk, def, spd) { }
    }
}
