﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Wizard : Hero
    {
        public float MagicAttack { get => magicAttack; set => magicAttack = value; }

        public Wizard()
        {
            magicAttack = 20;
        }

        public Wizard(float atk, float mgAtk, float def, float spd) : base(atk, def, spd) {
            this.magicAttack = mgAtk;
        }

        public float AttackMagic()
        {
            return magicAttack;
        }

        public override void SaveToFile(string fileToSaveTo)
        {
            base.SaveToFile(fileToSaveTo);
        }
    }
}
