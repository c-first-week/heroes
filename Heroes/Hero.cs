﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    public class Hero
    {
        protected float attack = 10;
        protected float defense = 8;
        protected float speed = 8;
        protected float magicAttack = 0;

        public float Attack {get => attack; set => attack = value;}
        public float Defense { get => defense; set => defense = value; }
        public float SpeedMpS { get => speed; set => speed = value; }

        public Hero() { }

        public Hero(float atk, float def, float spd)
        {
            this.attack = atk;
            this.defense = def;
            this.speed = spd;
        }

        public float Move()
        {
            return speed;
        }

        public float AttackMelee()
        {
            return attack;
        }

        public virtual void SaveToFile(string fileToSaveTo)
        {

        }

    }
}
